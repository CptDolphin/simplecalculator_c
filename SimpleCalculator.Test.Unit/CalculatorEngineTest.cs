﻿using System;
using SimpleCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleCalculator.Test.Unit
{
    [TestClass]
    public class CalculatorEngineTest
    {
        private readonly CalculatorEngine _calculatorEngine = new CalculatorEngine();

        [TestMethod]
        public void AddsTwoNumbersAndReturnsValidResultForNonSymbolOperation()
        {
            int number1 = 1;
            int number2 = 2;
            double result = _calculatorEngine.Calculate("+", number1, number2);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void AddsTwoNumbersAndReturnsValidResultForSymbolOperation()
        {
            int number1 = 1;
            int number2 = 2;
            double result = _calculatorEngine.Calculate("add", number1, number2);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void SubtractTwoNumbersAndReturnsValidResultForNonSymbolOperation()
        {
            int number1 = 5;
            int number2 = 2;
            double result = _calculatorEngine.Calculate("-", number1, number2);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void SubtractTwoNumbersAndReturnsValidResultForSymbolOperation()
        {
            int number1 = 5;
            int number2 = 2;
            double result = _calculatorEngine.Calculate("minus", number1, number2);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void MultiplyTwoNumbersAndReturnsValidResultForNonSymbolOperation()
        {
            int number1 = 1;
            int number2 = 2;
            double result = _calculatorEngine.Calculate("*", number1, number2);
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void MultiplyTwoNumbersAndReturnsValidResultForSymbolOperation()
        {
            int number1 = 1;
            int number2 = 2;
            double result = _calculatorEngine.Calculate("multiply", number1, number2);
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void DivideTwoNumbersAndReturnsValidResultForNonSymbolOperation()
        {
            int number1 = 6;
            int number2 = 3;
            double result = _calculatorEngine.Calculate("/", number1, number2);
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void DivideTwoNumbersAndReturnsValidResultForSymbolOperation()
        {
            int number1 = 6;
            int number2 = 3;
            double result = _calculatorEngine.Calculate("divide", number1, number2);
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void FailsToMakeOperationWithInvalidArgument()
        {
            int number1 = 5;
            int number2 = 10;
            double result = _calculatorEngine.Calculate("xxx", number1, number2);
        }


    }
}